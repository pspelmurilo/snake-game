package Snake;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Fruit {
    private int x;
    private int y;

    private Image image;

    public Fruit() {
        this.allocate();
        this.loadImage();
    }

    public void allocate(){
        int r = (int) (Math.random() * 45);
        setX(r*10);

        r = (int) (Math.random() * 45);
        setY(r*10);
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    protected void loadImage(){
        ImageIcon image = new ImageIcon("src/resources/apple.png");
        setImage(image.getImage());
    }

    public void eaten(Game.GameBoard gameBoard, Snake snake){
        snake.punctuate();
        snake.grow();
    }

    public boolean inBarrier(Barrier barrier1, Barrier barrier2, Barrier barrier3) {
        for (int i = 0; i < 25; i++) {
            if (this.getX() == barrier1.getX()[i] && this.getY() == barrier1.getY()[i]) {
                return true;
            }
        }
        for (int i = 0; i < 25; i++) {
            if (this.getX() == barrier2.getX()[i] && this.getY() == barrier2.getY()[i]) {
                return true;
            }
        }
        for (int i = 0; i < 10; i++) {
            if (this.getX() == barrier3.getX()[i] && this.getY() == barrier3.getY()[i]) {
                return true;
            }
        }
        return false;
    }

}
