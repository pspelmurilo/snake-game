package Snake;

import javax.swing.*;
import java.awt.*;

public class Snake {

    private final int DOT_SIZE = 10;

    private Image body;

    private int dots;
    private int points;

    private final int x[] = new int[2500];
    private final int y[] = new int[2500];

    private boolean leftDirection = false;
    private boolean rightDirection = true;
    private boolean upDirection = false;
    private boolean downDirection = false;

    public Snake() {
        loadImage();
        setDots(3);
        setPoints(0);
        for(int i=0; i<getDots(); i++){
            getX()[i] = 50 - i*10;
            getY()[i] = 50;
        }

    }

    public boolean isLeftDirection() {
        return leftDirection;
    }

    public void setLeftDirection(boolean leftDirection) {
        this.leftDirection = leftDirection;
    }

    public boolean isRightDirection() {
        return rightDirection;
    }

    public void setRightDirection(boolean rightDirection) {
        this.rightDirection = rightDirection;
    }

    public boolean isUpDirection() {
        return upDirection;
    }

    public void setUpDirection(boolean upDirection) {
        this.upDirection = upDirection;
    }

    public boolean isDownDirection() {
        return downDirection;
    }

    public void setDownDirection(boolean downDirection) {
        this.downDirection = downDirection;
    }


    public Image getBody() {
        return body;
    }

    public void setBody(Image body) {
        this.body = body;
    }


    public int getDots() {
        return dots;
    }

    public void setDots(int dots) {
        this.dots = dots;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int[] getX() {
        return x;
    }

    public int[] getY() {
        return y;
    }

    public void move() {

        for (int z = dots; z > 0; z--) {
            x[z] = x[(z - 1)];
            y[z] = y[(z - 1)];
        }

        if (leftDirection) {
            x[0] -= DOT_SIZE;
        }

        if (rightDirection) {
            x[0] += DOT_SIZE;
        }

        if (upDirection) {
            y[0] -= DOT_SIZE;
        }

        if (downDirection) {
            y[0] += DOT_SIZE;
        }
    }
    public boolean checkCollision() {

        for (int z = dots; z > 0; z--) {

            if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {
                return false;
            }
        }

        if (y[0] >= 500) {
            return false;
        }

        if (y[0] < 0) {
            return false;
        }

        if (x[0] >= 500) {
            return false;
        }

        if (x[0] < 0) {
            return false;
        }
        return true;
    }

    protected void loadImage(){
        ImageIcon image = new ImageIcon("src/resources/body.png");
        setBody(image.getImage());
    }

    public void punctuate(){
        this.setPoints(this.getPoints()+1);

    }

    public void grow(){
        this.setDots(this.getDots()+1);
    }

    public boolean checkBarrierCollision(Barrier barrier1, Barrier barrier2, Barrier barrier3){
        for(int i=0; i<25; i++){
            if(this.getX()[0] == barrier1.getX()[i] && this.getY()[0] == barrier1.getY()[i]){
                return false;
            }
        }
        for(int i=0; i<25; i++){
            if(this.getX()[0] == barrier2.getX()[i] && this.getY()[0] == barrier2.getY()[i]){
                return false;
            }
        }
        for(int i=0; i<10; i++){
            if(this.getX()[0] == barrier3.getX()[i] && this.getY()[0] == barrier3.getY()[i]){
                return false;
            }
        }
        return true;

    }

}
