package Snake;

import javax.swing.*;

public class Kitty extends Snake {
    @Override
    protected void loadImage() {
        ImageIcon image = new ImageIcon("src/resources/kitty.png");
        this.setBody(image.getImage());
    }

    @Override
    public boolean checkBarrierCollision(Barrier barrier1, Barrier barrier2, Barrier barrier3) {
        return true;
    }
}
