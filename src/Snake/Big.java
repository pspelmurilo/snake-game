package Snake;

import javax.swing.*;

public class Big extends Fruit {

    @Override
    protected void loadImage(){
        ImageIcon image = new ImageIcon("src/resources/big_fruit.png");
        setImage(image.getImage());
    }

    @Override
    public void eaten(Game.GameBoard gameBoard, Snake snake) {
        snake.punctuate();
        snake.punctuate();
        snake.grow();
    }
}
