package Snake;

import javax.swing.*;
import java.awt.*;

public class Barrier {
    private Image image;
    public int x[] = new int[25];
    public int y[] = new int[25];

    public Barrier(){
        image = new ImageIcon("src/resources/barrier.png").getImage();
    }

    public Image getImage() {
        return image;
    }

    public int[] getX() {
        return x;
    }

    public int[] getY() {
        return y;
    }


    public void setX(int i, int pix){
        x[i] = pix;
    }

    public void setY(int i, int pix){
        y[i] = pix;
    }

}
