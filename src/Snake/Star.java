package Snake;

import javax.swing.*;

public class Star extends Snake {

    @Override
    public void punctuate(){
        this.setPoints(this.getPoints()+2);
    }

    @Override
    protected void loadImage() {
        ImageIcon image = new ImageIcon("src/resources/star.png");
        this.setBody(image.getImage());
    }
}
