package Snake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Random;


public class Game extends JFrame {
    private JPanel Main;
    private JButton startGameButton;
    private JButton button2;
    private JButton button3;
    private JLayeredPane layeredPane1;
    private JPanel Menu;
    private JPanel PickSnake;
    private JButton Comum;
    private JButton Star;
    private JButton Kitty;

    private Snake snake;

    public Game(){
        setSize(500,500);
        setContentPane(Main);
        setLocationRelativeTo(null);
        startGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                layeredPane1.removeAll();
                layeredPane1.add(PickSnake);
                layeredPane1.repaint();
                layeredPane1.revalidate();
            }
        });
        Comum.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                snake = new Snake();
                GameBoard gameBoard = new GameBoard();
                setFocusable(true);
                setContentPane(gameBoard);
            }
        });

        Star.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                snake = new Star();
                GameBoard gameBoard = new GameBoard();
                setFocusable(true);
                setContentPane(gameBoard);
            }
        });


        setDefaultCloseOperation(EXIT_ON_CLOSE);
        Kitty.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                snake = new Kitty();
                GameBoard gameBoard = new GameBoard();
                setFocusable(true);
                setContentPane(gameBoard);
            }
        });
    }

    public class GameBoard extends JPanel implements ActionListener{


        private Barrier barrier1;
        private Barrier barrier2;
        private Barrier barrier3;

        private Fruit good_fruit;
        private Fruit bad_fruit;
        private Timer timer;
        public boolean inGame;
        private Image barrier = new ImageIcon("src/resources/barrier.png").getImage();

        private InputMap im = getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW);
        private ActionMap am = this.getActionMap();

        public GameBoard(){
            initBoard();
        }

        private void initBoard(){

            setBackground(Color.BLACK);
            setFocusable(true);
            requestFocusInWindow();
            inGame = true;
            setSize(new Dimension(500, 500));
            setVisible(true);
            initGame();

            im.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "RightArrow");
            im.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "LeftArrow");
            im.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "UpArrow");
            im.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "DownArrow");

            am.put("RightArrow", new ArrowAction("RightArrow"));
            am.put("LeftArrow", new ArrowAction("LeftArrow"));
            am.put("UpArrow", new ArrowAction("UpArrow"));
            am.put("DownArrow", new ArrowAction("DownArrow"));
        }

        private void initGame(){
            int random = (int)Math.random() * 2;

            good_fruit = new Fruit();
            bad_fruit = new Bomb();
            barrier1 = new Barrier();
            for(int i=0; i < 25; i++) {
                barrier1.setX(i, 80);
                barrier1.setY(i, 120 + i * 10);
            }
            barrier2 = new Barrier();
            for(int i=0; i < 25; i++) {
                barrier2.setX(i, 410);
                barrier2.setY(i, 120 + i * 10);
            }

            barrier3 = new Barrier();
            for(int i=0; i < 10; i++) {
                barrier3.setX(i, 200 + i * 10);
                barrier3.setY(i, 250);
            }
            while (good_fruit.inBarrier(barrier1, barrier2, barrier3)) {
                good_fruit.allocate();
            }

            timer = new Timer(70, this);
            timer.start();



        }


        @Override
        public void paintComponent(Graphics g){
            super.paintComponent(g);

            doDrawing(g);
        }

        private void doDrawing(Graphics g) {
            if (inGame) {

                String msg = "Points: " + snake.getPoints();
                Font small = new Font("Helvetica", Font.BOLD, 14);
                FontMetrics metr = getFontMetrics(small);

                g.setColor(Color.white);
                g.setFont(small);
                g.drawString(msg, (500 - metr.stringWidth(msg)) / 2, 50);

                g.drawImage(good_fruit.getImage(), good_fruit.getX(), good_fruit.getY(), this);
                g.drawImage(bad_fruit.getImage(), bad_fruit.getX(), bad_fruit.getY(), this);

                for(int i=0; i < 25; i++) {
                    g.drawImage(barrier1.getImage(), barrier1.getX()[i], barrier1.getY()[i], this);
                }
                for(int i=0; i < 25; i++) {
                    g.drawImage(barrier2.getImage(), barrier2.getX()[i], barrier2.getY()[i], this);
                }
                for(int i=0; i < 10; i++) {
                    g.drawImage(barrier3.getImage(), barrier3.getX()[i], barrier3.getY()[i], this);
                }
                for (int i = 0; i < snake.getDots(); i++) {
                    g.drawImage(snake.getBody(), snake.getX()[i], snake.getY()[i], this);
                }
                Toolkit.getDefaultToolkit().sync();

            } else {
                gameOver(g);
            }
        }

        private void gameOver(Graphics g){
            String msg = "Game Over";
            Font small = new Font("Helvetica", Font.BOLD, 14);
            FontMetrics metr = getFontMetrics(small);

            g.setColor(Color.white);
            g.setFont(small);
            g.drawString(msg, (500 - metr.stringWidth(msg)) / 2, 500 / 2);
        }

        private void checkCollision(){
            inGame = snake.checkCollision() && snake.checkBarrierCollision(barrier1, barrier2, barrier3) ;
            if(!inGame){
                timer.stop();
            }
        }

        private void checkFruit() {
            if (good_fruit.getX() == snake.getX()[0] && good_fruit.getY() == snake.getY()[0]) {
                good_fruit.eaten(this, snake);
                Random r = new Random();
                int random = r.nextInt(3);
                if(random == 0) {
                    good_fruit = new Fruit();
                }
                else if(random == 1){
                    good_fruit = new Big();
                }
                else{
                    bad_fruit = new Bomb();
                }
                while (good_fruit.inBarrier(barrier1, barrier2, barrier3)) {
                    fruit.allocate();
                }
            }
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if(inGame){

                checkCollision();
                snake.move();
                checkFruit();
            }

            repaint();
        }

        public class ArrowAction extends AbstractAction {

            private String cmd;

            public ArrowAction(String cmd) {
                this.cmd = cmd;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                if (cmd.equalsIgnoreCase("LeftArrow") && (!snake.isRightDirection())) {
                    snake.setLeftDirection(true);
                    snake.setUpDirection(false);
                    snake.setDownDirection(false);
                } else if (cmd.equalsIgnoreCase("RightArrow") && (!snake.isLeftDirection())) {
                    snake.setRightDirection(true);
                    snake.setUpDirection(false);
                    snake.setDownDirection(false);
                } else if (cmd.equalsIgnoreCase("UpArrow") && (!snake.isDownDirection())) {
                    snake.setUpDirection(true);
                    snake.setRightDirection(false);
                    snake.setLeftDirection(false);
                } else if (cmd.equalsIgnoreCase("DownArrow") && (!snake.isUpDirection())) {
                    snake.setDownDirection(true);
                    snake.setRightDirection(false);
                    snake.setLeftDirection(false);
                }
            }
        }

    }

}
