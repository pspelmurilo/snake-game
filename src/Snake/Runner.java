package Snake;

import javax.swing.*;
import java.awt.*;

public class Runner {

    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {
            JFrame ex = new Game();
            ex.setVisible(true);
        });
    }

}
