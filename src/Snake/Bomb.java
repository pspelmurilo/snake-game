package Snake;

import javax.swing.*;

public class Bomb extends Fruit {
    @Override
    protected void loadImage() {
        ImageIcon image = new ImageIcon("src/resources/bomb_fruit.png");
        this.setImage(image.getImage());
    }

    @Override
    public void eaten(Game.GameBoard gameBoard, Snake snake) {
        gameBoard.inGame = false;
    }
}
